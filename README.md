# Setup in package to be published

In the `name:` field in `package.json`, be sure to scope the name with `@aae/` - EX:  `@aae/mypackagename`

Add the following to the `package.json` in the project directory
```json
"publishConfig": { 
  "@aae:registry": "https://git.aae.wisc.edu/api/v4/projects/172/packages/npm/" 
}
```



In order to publish to the registry, you must create a project access token.
<a href="https://git.aae.wisc.edu/aae/npm-registry/-/settings/access_tokens" target="_blank">https://git.aae.wisc.edu/aae/npm-registry/-/settings/access_tokens</a>

Add the token to the `%USERPROFILE%\.npmrc`.   **DO NOT ADD TO PACKAGE `.npmrc`!!  SECURITY RISK**
```
@aae:registry=https://git.aae.wisc.edu/api/v4/packages/npm/
//git.aae.wisc.edu/api/v4/projects/172/packages/npm/:_authToken=TOKENHERE
```

# Setup projects to consume the AAE NPM registry
Add the following to `.npmrc` file.   This file can be located in the local project directory, or in the user profile location `%USERPROFILE%\.npmrc`
```
@aae:registry=https://git.aae.wisc.edu/api/v4/packages/npm/
```
